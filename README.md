# A React Task
In this task, we are provided with a **form** which we have to animate with Framer Motion. 
## Objective
* Checkout the dev branch
*Add an **enter animation** on the form using Framer Motion.
* The enter animation should be the **X** axis. It can beeither from the left or right.
* After we have set the initial animation, we have to make sure that the form sets itself in the center of the viewport.
## Requirements
* The project starts with **npm run start**.
* Intall Framer Motion - **npm install framer-motion**
* When implemented merge dev branch into master.
## Importing
* **import { motion } from "framer-motion"**

## Gotchas
Learn more about the **Framer Motion library** here - https://www.framer.com/docs/